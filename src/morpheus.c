/* morpheus.c
 *
 * Copyright 2019 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "matrix.h"
#include "morpheus.h"

#include <getopt.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define OPTSTR "d:f:hi:m:n:t:v"

const char *prog;

int
get_dimension (int  *m,
               int  *n,
               char *expr)
{
  *m = *n = 1;

  while (*expr && *expr++ != ';')
    {
      if (*expr == ',')
        ++*n;
    }

  if (!*expr)
    return 0;

  for (int i = 0; *expr; ++expr)
    {
      if (*expr == ',')
        ++i;

      if (*expr == ';' || *expr == ']')
        {
          ++*m;

          if (i % (*n - 1))
            return -1;
        }
    }

  return 0;
}

void
usage ()
{
  printf ("Usage: %s MODE EXPRESSION\n"
          "Perform various operations on matrices.\n\n"
          "  -d\tCalculate the determinant of matrix A\n"
          "  -f\tFormat matrix A\n"
          "  -h\tPrint help\n"
          "  -i\tCalculate the inverse of matrix A\n"
          "  -t\tTranspose matrix A\n"
          "  -v\tPrint program version\n\n"
          "Where A is an expression in the form:\n"
          "[a11, a12, a13; a21, a22, a23; a31, a32, a32]\n", prog);
}

void
die (const char *err,
     ...)
{
  va_list ap;

  va_start (ap, err);
  vfprintf (stderr, err, ap);
  va_end (ap);

  exit (EXIT_FAILURE);
}

int
main (int   argc,
      char *argv[])
{
  int opt;
  int mode;
  int m;
  int n;
  Matrix *a = NULL;

  prog = basename (*argv);

  if ((opt = getopt (argc, argv, OPTSTR)) == -1 || argc > 3)
    die ("Usage: %s MODE EXPRESSION\n", prog);

  switch (opt)
    {
    case 'd':
      mode = DETERMINANT;
      break;
    case 'f':
      mode = FORMAT;
      break;
    case 'h':
      usage ();
      return EXIT_SUCCESS;
    case 'i':
      mode = INVERSE;
      break;
    case 't':
      mode = TRANSPOSE;
      break;
    case 'v':
      printf ("%s %s\n", prog, VERSION);
      return EXIT_SUCCESS;
    case '?':
      die ("Try '%s -h' for more information\n", prog);
    }

  if (get_dimension (&m, &n, optarg) == -1)
    die ("%s: Matrices must be rectangular\n", prog);

  a = matrix_alloc (m, n);

  matrix_parse (a, optarg);
  matrix_proc (a, mode);

  matrix_free (a);

  return EXIT_SUCCESS;
}
