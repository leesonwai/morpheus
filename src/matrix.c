/* matrix.c
 *
 * Copyright 2019 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "matrix.h"
#include "morpheus.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
get_width (Matrix *a)
{
  int w = 0;
  int len;

  for (int i = 0; i < a->m; ++i)
    {
      for (int j = 0; j < a->n; ++j)
        {
          if ((len = snprintf (NULL, 0, "%g", a->a[i][j])) > w)
            w = len;
        }
    }

  return (w + 1);
}

Matrix *
get_transposed (Matrix *a)
{
  Matrix *b = matrix_alloc (a->n, a->m);

  for (int i = 0; i < b->m; ++i)
    {
      for (int j = 0; j < b->n; ++j)
        b->a[i][j] = a->a[j][i];
    }

  return b;
}

void
matrix_print (Matrix *a)
{
  int w = get_width (a);

  for (int i = 0; i < a->m; ++i)
    {
      for (int j = 0; j < a->n; ++j)
        printf ("%+-*g%c", w, a->a[i][j], (j == a->n - 1) ? '\n' : ' ');
    }
}

float
laplace (Matrix *a)
{
  Matrix *b = NULL;
  float det = 0;

  if (a->m != a->n)
    die ("%s: Matrix must be square to calculate determinant\n", prog);

  if (a->m == 1)
    return a->a[0][0];

  for (int i = 0; i < a->n; ++i)
    {
      b = matrix_alloc (a->m - 1, a->n - 1);

      for (int j = 1; j < a->m; ++j)
        {
          for (int k = 0, l = 0; k < a->n; ++k)
            {
              if (k == i)
                continue;

              b->a[j - 1][l++] = a->a[j][k];
            }
        }

      det += pow (-1, i) * a->a[0][i] * laplace (b);
      matrix_free (b);
    }

  return det;
}

float
get_minor (Matrix *a,
           int     i,
           int     j)
{
  Matrix *b = NULL;
  float minor = 0;

  if (a->m == 1)
    return a->a[0][0];

  b = matrix_alloc (a->m - 1, a->n - 1);

  for (int k = 0, m = 0; k < a->m; ++k)
    {
      if (k == i)
        continue;

      for (int l = 0, n = 0; l < a->n; ++l)
        {
          if (l == j)
            continue;

          b->a[m][n++] = a->a[k][l];
        }
      ++m;
    }

  minor = laplace (b);
  matrix_free (b);

  return minor;
}

Matrix *
get_inverse (Matrix *a)
{
  float det;
  float rdet;
  Matrix *b = NULL;
  Matrix *c = NULL;

  if (a->m != a->n)
    die ("%s: Matrix must be square to calculate inverse\n", prog);
  if (!(det = laplace (a)))
    die ("%s: Matrix is singular; no inverse\n", prog);

  rdet = 1 / det;
  b = matrix_alloc (a->m , a->n);

  for (int i = 0; i < b->m; ++i)
    {
      for (int j = 0; j < b->n; ++j)
        b->a[i][j] = rdet * (pow (-1, i + j) * get_minor (a, i, j));
    }

  c = get_transposed (b);
  matrix_free (b);

  return c;
}

void
matrix_free (Matrix *a)
{
  for (int i = 0; i < a->m; ++i)
    free (a->a[i]);
  free (a);
}

void
matrix_proc (Matrix *a,
             int     mode)
{
  float det;
  Matrix *b = NULL;

  switch (mode)
    {
    case DETERMINANT:
      det = laplace (a);
      printf ("%g\n", det);
      break;
    case FORMAT:
      matrix_print (a);
      break;
    case INVERSE:
      b = get_inverse (a);
      matrix_print (b);
      break;
    case TRANSPOSE:
      b = get_transposed (a);
      matrix_print (b);
      break;
    }

  if (b)
    matrix_free (b);
}

void
matrix_parse (Matrix *a,
              char   *expr)
{
  char *cur;
  float f;

  if (*expr++ != '[')
    die ("%s: Missing opening bracket\n", prog);

  for (int i = 0; i < a->m; ++i)
    {
      for (int j = 0; j < a->n; ++j)
        {
          f = strtof (expr, &cur);

          if (expr == cur)
            die ("%s: Syntax error at '%c'\n", prog, *cur);

          a->a[i][j] = f;
          expr = (*cur) ? cur + 1 : cur;
        }
    }

  if (*--expr != ']')
    die ("%s: Missing closing bracket\n", prog);
}

Matrix *
matrix_alloc (int m,
              int n)
{
  Matrix *a = malloc (sizeof (Matrix) + sizeof (float *) * m);

  if (!a)
    die ("%s: Insufficient memory available\n", prog);

  for (int i = 0; i < m; ++i)
    {
      if (!(a->a[i] = malloc (sizeof (float) * n)))
        die ("%s: Insufficient memory available\n", prog);
    }
  a->m = m;
  a->n = n;

  return a;
}
