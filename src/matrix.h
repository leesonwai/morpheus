/* matrix.h
 *
 * Copyright 2019 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

typedef struct
{
  int    m;
  int    n;
  float *a[];
} Matrix;

Matrix *  matrix_alloc  (int m,
                         int n);

void      matrix_parse  (Matrix *a,
                         char   *expr);

void      matrix_free   (Matrix *a);

void      matrix_proc   (Matrix *a,
                         int     mode);
