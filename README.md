# morpheus
### A small utility for working with matrices
![License: GPL3](https://img.shields.io/badge/license-GPL3-green?style=flat-square)

`morpheus` is a simple command-line utility that performs basic operations on
matrices.

An example usage of `morpheus` to transpose a matrix: `morpheus -t '[1, 2, 3; 4, 5, 6; 7, 8, 9]'`

## Installing
```
meson builddir
cd builddir/
ninja
sudo ninja install
```
